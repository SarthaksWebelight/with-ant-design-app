import { PlusOutlined, LeftOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import React, { useRef } from 'react';
import { Table } from 'antd';
import { TableComponentProps } from './type';
import "./table.module.less";

const TableList: React.FC<TableComponentProps> = (props: TableComponentProps) => {
  const {
    back,
    addNew,
    tabData,
    columns,
    loading,
    ifExpandedRow = false,
    expandableRow,
    onChange,
    onReset,
    pagination,
  } = props;
  return (
      <Table
        onReload={onReset}
        rowKey="id"
        search={false}
        // toolBarRender={() => []}
        loading={loading}
        dataSource={tabData}
        columns={columns}
        onChange={onChange}
        pagination={pagination}
        expandable={{
          expandedRowRender: (record: any) => {
            return (
              expandableRow &&
              expandableRow.map((expandableRowData: any) => {
                return (
                  record.id === expandableRowData.id &&
                  expandableRowData.body.map((expandableRowBody: any) => (
                    <div
                      key={expandableRowData.id}
                      style={{
                        display: 'flex',
                        paddingLeft: 108,
                        paddingRight: 300,
                      }}
                    >
                      <p>{expandableRowBody.title}</p>&nbsp;:&nbsp;
                      <p>{expandableRowBody.desc}</p>
                    </div>
                  ))
                );
              })
            );
          },
          rowExpandable: () => ifExpandedRow,
        }}
      />
  );
};

export default TableList;
