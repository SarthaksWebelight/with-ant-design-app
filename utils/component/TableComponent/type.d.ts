import { ReactNode } from 'react';
// import { ProColumns } from '@ant-design/pro-table';

export interface IColumnData {
  title?: string;
  dataIndex?: string;
  render?: ReactNode | any;
  filters?: string;
}
export type ISortOrder = 'descend' | 'ascend' | null;
export interface Pagination {
  current: number;
  pageSize: number;
  total?: number;
}

export interface TableComponentProps {
  back?: string;
  addNew?: string;
  columns: IColumnData[];
  tabData?: string[];
  loading?: boolean;
  ifExpandedRow?: boolean;
  expandableRow?: any;
  onChange?: (pagination: any, filters: any, sorter: any) => void;
  onReset?: () => void;
  pagination?: Pagination;
}
