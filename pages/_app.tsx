import React, { ReactNode, useEffect } from "react"
import { AppProps } from "next/app"
import { ApolloClient, ApolloProvider } from "@apollo/react-hooks"
import { setContext } from "@apollo/client/link/context"
import { onError } from "@apollo/client/link/error"
import { InMemoryCache, createHttpLink, ApolloLink } from "@apollo/client"
// import { getLocalStorageItem, localStorageClear } from "../helper/utils"
// import { MessageComponent } from "../helper/components/FormComponent"
// import "../styles/globals.scss"
// import "antd/dist/antd.css"
// import "react-quill/dist/quill.snow.css"
// import 'bootstrap/dist/css/bootstrap.min.css';

export const adminCache = new InMemoryCache({})

const httpLink = createHttpLink({
   uri: process.env.REACT_APP_BASE_URL, // REACT_APP_BASE_URL
})
const errorLink = onError((error) => {
   const { graphQLErrors } = error

   if (graphQLErrors)
      graphQLErrors.map((errorGraphql: any) => {
         if (errorGraphql.extensions.code === "UNAUTHENTICATED") {
            localStorageClear()
         }
         return MessageComponent({
            type: "error",
            messageValue: errorGraphql.message,
         })
      })
})
const authLink = setContext((_, { headers }) => {
   if (typeof window !== "undefined") {
      const token = localStorage.getItem("auth_token")
      return {
         headers: {
            ...headers,
            authorization: token ? `Bearer ${token}` : "",
         },
      }
   }
})

export const client = new ApolloClient({
   cache: adminCache,
   link: ApolloLink.from([errorLink, authLink.concat(httpLink)]),
})

const ApollowLayout: React.FC<AppProps> = (props: AppProps) => {
   const { Component, pageProps } = props
   //  const token = getLocalStorageItem("auth_token")
   let token

   if (typeof window !== "undefined") {
      const token = localStorage.getItem("auth_token")
   }
   useEffect(() => {
      if (!token) {
         // history.push(ROUTE_PATH.loginScreen);
      }
   }, [])
   return (
      <ApolloProvider client={client}>
         <Component {...pageProps} />
      </ApolloProvider>
   )
}

export default ApollowLayout